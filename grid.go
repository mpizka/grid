// Package grid provides abstractions for creating and manipulating 2D grids
// of bytes frequently used in e.g. terminal applications and implementations
// of cellular automata or games
//
// Grids have a width (w) and height (h). Their elements are located by grid
// coordinates (x, y) with x being width. The origin (0, 0) is the upper-left.
// 
//   (0, 0) ------------- (w-1, 0)
//        |               |
// (0, h-1) ------------- (w-1, h-1) 

package grid

import (
    "bytes"
)

func must(b bool, s string) { if !b { panic(s) } }

type Grid struct {
    w int           // width
    h int           // height
    def byte        // default byte value
    data []byte
}

// NewGrid returns a pointer to new Grid of width w height h with default 0
func NewGrid(w, h int, ch byte) *Grid {
    l := h*w
    data := make([]byte, l, l)
    g := Grid{w, h, ch, data}
    g.Reset()
    return &g
}


// FromString returns a new grid of width w initialized to s
// Only printable runes within ascii range are used
// everything else is replaced by "_". Grid is padded with spaces.
func FromString(s string, w int) *Grid {
    l := len([]rune(s))
    // determine h
    h := l / w
    if l % w > 0 { h++ }
    g := NewGrid(w, h, ' ')
    g.Reset()
    g.WriteStringOffset(s, '_', 0)
    return g
}


// WriteString writes ASCII string s into the grid, replacing non-ASCII
// runes with ch. Depending on grid-size, s may be truncated.
// panics if ch is non-ASCII
func (g *Grid) WriteString(s string, ch byte) {
    g.WriteStringOffset(s, ch, 0)
}

// WriteStringOffset writes ASCII string s into the grid from offset 0
// replaceing non-ASCII runes with ch. s may be truncated. Panics if ch
// is non-ASCII.
func (g *Grid) WriteStringOffset(s string, ch byte, o int) {
    must(ch < 127 && ch > 31, "bad ch")

    asrunes := []rune(s)
    offsetData := g.data[o:]

    l := len(offsetData)
    if len(asrunes) < l {
        l = len(asrunes)
    }

    for i := 0; i < l; i++ {
        r := asrunes[i]
        if r > 126 || r < 32 {
            offsetData[i] = ch
        } else {
            offsetData[i] = byte(r)
        }
    }
}


// Fill sets all values of grid to b
func (g *Grid) Fill (b byte) {
    for i,_ := range g.data {
        g.data[i] = b
    }
}


// Reset sets all values in grid to its default
func (g * Grid) Reset() {
    g.Fill(g.def)
}


// Dimensions returns a Grids Dimensions (width, height, length)
func (g *Grid) Dimensions () (int, int, int) {
    return g.w, g.h, len(g.data)
}


// Len returns the length of the grid in bytes
func (g *Grid) Len() int {
    return len(g.data)
}

// Default changes the grids default value to d
func (g *Grid) Default(d byte) {
    g.def = d
}


// Index returns idx of underlying data element of Grid at x/y
func (g *Grid) Index (x,y int) int {
    return y * g.w + x
}


// Pos returns x/y position of element stored at idx of Grids underlying data
func (g *Grid) Pos (idx int) (int, int) {
    return idx % g.w, idx / g.w
}


// Set sets grid element at x/y to b
func (g *Grid) Set (x, y int, b byte) {
    g.data[g.Index(x, y)] = b
}


// Get returns grid element value at x/y
func (g *Grid) Get (x, y int) byte {
    return g.data[g.Index(x, y)]
}


// TODO: do we need this? Is this useful for anything?
//       If the purpose is sending/storing the grid somewhere, wouldn't
//       a copy of the underlying array be better?
// Data returns slice of grids underlying data
// Slice is valid until at least the next grid manipulation
func (g *Grid) Data () []byte {
    return g.data[:]
}


// CopyRegion returns a pointer to a new grid, representing a rectangular
// cutout of src defined by upperLeftX/Y (ux/uy) and lowerRightX/Y (lx/ly)
func (g *Grid) CopyRegion (ux, uy, lx, ly int) *Grid {
    w := (lx - ux) + 1
    h := (ly - uy) + 1
    l := w*h
    ng := NewGrid(w, h, g.def)
    for i:=0; i<l; i++ {
        x,y := ng.Pos(i)
        ng.data[i] = g.Get(x+ux,y+uy)
    }
    return ng
}


// Paste pastes src (Grid) values into Grid at pos x/y
func (g *Grid) Paste (src *Grid, x, y int) {
    sl := src.w * src.h
    for i := 0; i < sl; i++ {
        sx, sy := src.Pos(i)
        //g.Set(x+sx, y+sy, src.data[i])
        g.Set(x+sx, y+sy, src.Get(sx, sy))
    }
}



// String returns a string-representation of grid insertig pre/post
// before/after each line respectively
func (g *Grid) String(pre, post string) string {
    bpre := []byte(pre)
    bpst := []byte(post)
    l := g.w * g.h
    var buf bytes.Buffer

    buf.Write(bpre)
    for i:=0; i<l; i++ {
        if i > 0 && i < l-1 && i % g.w == 0 {
            buf.Write(bpst)
            buf.Write(bpre)
        }
        buf.WriteByte(g.data[i])
    }
    buf.Write(bpst)

    return buf.String()
}
